# Mikrotik monitoring UPS + Mikrotik Log + Telegram

## Отслеживаем состояние источника бесперебойного питания который подключен к Mikrotik'у

### Подготовка:

1. Mikrotik - RouterOS **v7.5** (на данной версии проводилось тестирование).
2. Mikrotik - установлен пакет **USP**.
3. Mikrotik - в настройках **RouterOS** добавлен источник бесперебойного питания.
4. Telegram - есть **Bot Token** и **Chat ID**.

### Настройка Mikrotik'а

1. Создадим скрипт для **Telegram Bot'a**:\
**Name** - Variables_Telegram\
**Don't Require Permissions** - да\
**Policy** - нет\
**Source** - [Ссылка на файл](/Scripts/Variables_Telegram.rsc)

Для работы скрипта необходимо указать свои значения переменных:
```
:local myBotToken "%token%"
:local myChatID "%id%"
```

![ALT](/Screenshots/mikrotik_variables_telegram.jpg)

2. Создадим скрипт для отслеживания состояния **UPS**:\
**Name** - Monitoring_UPS\
**Don't Require Permissions** - нет\
**Policy** - read, write, policy, test\
**Script** - [Ссылка на файл](/Scripts/Monitoring_UPS.rsc)

Получить список всех источников бесперебойного питания:
```
/system ups print brief
```

Для работы скрипта необходимо указать свои значения переменных:
```
:local myUpsName "%ups_name%"
:local myUPSBatteryChargeLevelDropeBelow "20"
:local myUPSVoltageUpperLineLimit "241"
:local myUPSVoltageLowerLineLimit "214"
:local myDelaySendNotifi "3"
```

![ALT](/Screenshots/mikrotik_monitoring_ups.jpg)

3. Создадим задание в планировщике:\
**Name** - Script_Monitoring_UPS\
**Start Date** - Jan/01/2000\
**Start Time** - 00:00:00\
**Interval** - 00:00:05 (частота запуска на ваше усмотрение)\
**Policy** - read, write, policy, test\
**On Event** - /system script run "Monitoring_UPS"

![ALT](/Screenshots/mikrotik_schedule_monitoring_ups.jpg)


### Примеры работы скрипта:

**Mikrotik -> System -> Scripts -> Environment**\
![ALT](/Screenshots/mikrotik_monitoring_ups_env_info.jpg)

**Mikrotik -> Log**\
![ALT](/Screenshots/mikrotik_monitoring_ups_logs.jpg)

**Telegram:**\
Источник бесперебойного питания подключен.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_ups_connected.jpg)

Источник бесперебойного питания отключен.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_ups_disconnected.jpg)

Сбой питания.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_power_failure.jpg)

Питание восстановлено.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_power_restore.jpg)

Уровень заряда батареи упал ниже порогового значения.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_battery_level_dropped_below.jpg)

Батарея полностью заряжена.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_battery_fully_charged.jpg)

Высокое входное напряжение.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_high_input_voltage.jpg)

Низкое входное напряжение.\
![ALT](/Screenshots/mikrotik_monitoring_ups_telegram_low_input_voltage.jpg)



