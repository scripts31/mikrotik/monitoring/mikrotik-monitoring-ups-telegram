#
#Variables for Telegram Bot
#Written by TooMooN
#RouterOS v7.5
#Version 1.0
#

#Bot token
:local myBotToken "%token%"

#Chat ID
:local myChatID "%id%"

#Message
:local myTgMessage [$myTgMessageText]

#Telegram API URL
:local myTgUrl "https://api.telegram.org/bot$myBotToken/sendMessage?chat_id=$myChatID&text=$myTgMessage"

/tool fetch url=$myTgUrl keep-result=no
